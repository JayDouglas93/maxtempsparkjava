
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.*;

import scala.Tuple2;

import org.apache.spark.SparkConf;


public class MaxTempSpark {

	public static void main(String[] args) throws Exception{
		
		if (args.length != 2) {
			System.err.println("Please specify input and output path");
			System.exit(-1);
		}
		
		SparkConf conf = new SparkConf();
		JavaSparkContext sc = new JavaSparkContext("local", "MaxTempSpark", conf);
		JavaRDD<String> lines = sc.textFile(args[0]);
		
		JavaRDD<String[]> records = lines.map(new Function<String, String[]>(){
			
			@Override 
			public String[] call(String s) {
				return s.split("\t");
			}
		});
		
		JavaRDD<String[]> filtered = records.filter(new Function<String[], Boolean>(){
			
			@Override
			public Boolean call(String[] rec) {
				return rec[1] != "9999" && rec[2].matches("[01459]");
			}
			
		});
		
		JavaPairRDD<Integer, Integer> tuples = filtered.mapToPair(
				
			new PairFunction<String[], Integer, Integer>(){
					
				@Override
				public Tuple2<Integer, Integer> call(String[] rec){
					return new Tuple2<Integer, Integer>(Integer.parseInt(rec[0]), Integer.parseInt(rec[1]));	
				}
		});
		
		JavaPairRDD<Integer, Integer> maxTemps = tuples.reduceByKey(
				new Function2<Integer, Integer, Integer>(){
					@Override
					public Integer call(Integer i1, Integer i2) {
						return Math.max(i1, i2);
					}
				});
		
		maxTemps.saveAsTextFile(args[1]);
	}
	
}
